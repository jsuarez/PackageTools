- Package tools
- Created by Javier Suarez on 17.8.2015
- Version 1.0


PREREQUISITES:

- Java. In my example JDK 1.7.0_75
- Apache Maven 3. In my example 3.2.5
- Heroku Account
- Heroku Toolbelt
- Git client



DOWNLOAD SOURCE:
1. Clone the git repository into your local machine
	$ git clone https://gitlab.com/jsuarez/PackageTools.git

DEPLOY LOCAL	
1. Open a terminal and go to the directory where the "pom.xml" file is
2. Package your application
	$ mvn clean package
3. Test it locally
	$ foreman start web
4. Open browser and go to http://localhost:500


DEPLOY IN HEROKU
1. Log in your Heroku account
	$ heroku login
2. Create a Git repository for your project
	$ git init
3. Add the resources to the repository
	$ git add .	
4. Commit your application into the repository
	$ git commit -m "Initial import" 
5. Create an Heroku application
	$ heroku create
6. Push your data into the Heroku server
	$ git push heroku master
7. Test your application
	$ heroku open

GUIDE OF USE
1. Open  the browser and browse to the app
2. There are two sections. One if for compare two packages the other is to extract the elements from a package is CSV format
3. Compare
	You have to pick the both package you want to compare and the click in the compare button. Once the comparison is finished you will see in the page a CSV with the comparison results. There are also a button to download the CSV to your local machine.
	The format of this CSV is the following:	
	1. type - Indicates the type of element
	2. name - Indicates the name of the element
	3. comparision:
		- 0 if the element exist in origin but not in destination
		- 1 if the element exist in origin and in destination
		- 2 if the element exist in origin and in destination but doesn't match the case
	4. origin - The file that is being analysing. package1.xml for the first picked. package2 for the second picked
4. Extract
	You have to pick the package you want to extract and the click in the extract button. Once the extraction is finished you will see in the page a CSV with the extraction results. There are also a button to download the CSV to your local machine.
	The format of this CSV is the following:	
	1. type - Indicates the type of element
	2. name - Indicates the name of the element
	



