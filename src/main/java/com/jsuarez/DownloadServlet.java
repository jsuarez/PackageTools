/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jsuarez;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author jsuarez
 */
public class DownloadServlet extends HttpServlet {

    public void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException {
        
        response.setContentType("text/plain");
        response.setHeader("Content-Disposition",
                "attachment;filename=result.csv");

        OutputStream os = response.getOutputStream();

        String prePath = System.getenv("FILE_PATH");

        BufferedReader br = new BufferedReader(new FileReader(prePath + "result.csv"));
        String linea = null;

        while ((linea = br.readLine()) != null) {
            linea += "\n";
            byte[] bytes = linea.getBytes();
            os.write(bytes);
        }

        os.flush();
        os.close();
    }

}
