/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jsuarez;

import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author jsuarez
 */
public class FileUploadHandler extends HttpServlet {    
  
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String prePath = (System.getenv("FILE_PATH")==null)?"/app/":System.getenv("FILE_PATH");
        //process only if its multipart content
        if(ServletFileUpload.isMultipartContent(request)){
            try {
                FileItemFactory factory = new DiskFileItemFactory();
                ServletFileUpload upload = new ServletFileUpload(factory);
              
                List items = null;
 
                try {
                    // parse this request by the handler
                    // this gives us a list of items from the request
                    items = upload.parseRequest(request);
                    log("items: " + items.toString());
                } catch (FileUploadException ex) {
                    log("Failed to parse request", ex);
                }
                boolean sw = true;
                for(Object o: items){
                    FileItem item = (FileItem) o;
                    if(!item.isFormField()){
                        System.out.println("****"+item.getFieldName());
                        if (sw){
                            item.write( new File(prePath + "package1.xml"));
                            sw = false;
                        }
                        else{
                            item.write( new File(prePath+ "package2.xml"));
                        }
                    }
                }
           
               //File uploaded successfully
               request.setAttribute("message", "File Uploaded Successfully");
            } catch (Exception ex) {
               request.setAttribute("message", "File Upload Failed due to " + ex);
            }          
         
        }else{
            request.setAttribute("message",
                                 "Sorry this Servlet only handles file upload request");
        }
            
        request.getRequestDispatcher("/result.jsp").forward(request, response);
     
    }
  
}



