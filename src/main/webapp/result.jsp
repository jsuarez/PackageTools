<%@page import="java.io.BufferedReader" %>
<%@page import="java.io.FileReader" %>
<%@page import="com.jsuarez.packagecomparer.PackageTools" %>

<html>
    <head>
        <link href="css/main.css" type="text/css" rel="stylesheet"/>
    </head>
<body>
    <h2>Comparison Result</h2>
    <p>
        <button onclick="window.location.href='/download'">Download CSV</button>
        <button onclick="window.location.href='/index.jsp'">Back</button>
    </p>
    <div>
<%
    String prePath = (System.getenv("FILE_PATH")==null)?"/app/":System.getenv("FILE_PATH");
    PackageTools.compare(prePath+"package1.xml",
                         prePath+"package2.xml", 
                         prePath+"result.csv");

    BufferedReader br = new BufferedReader(new FileReader(prePath+"result.csv"));
    String linea = null;

    while ( (linea = br.readLine()) != null){
        out.println(linea+"<br/>");
    }

%>        
    </div>
</body>
</html>